/* binomialcoefficient.c
	Author: David Warren II
	Date: 20 February, 2017
	Description: Calculates the binomial coefficient from 2 numbers
*/

#include <stdio.h>

long int factorial(int number);

int main()
{
	int n = 0;
	int k = 0;
	
	printf("This program only accepts integers.\n");
	printf("Enter the total amount of possibilities: ");
	scanf("%d",&n);
	printf("\nEnter the amount of possibilies that are wanted: ");
	scanf("%d", &k);

	printf("\nThe binomial coefficient is: %ld\n", factorial(n) / (factorial(k) * (factorial(n - k))));

	return 0;
}

long int factorial(int number)
{
	if(number > 1)
		return number * factorial(number - 1); 
	else
		return 1; 
}
